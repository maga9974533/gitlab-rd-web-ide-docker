FROM ubuntu:22.10

WORKDIR gitlab-rd-web-ide

COPY ./scripts scripts
COPY ./nginx.conf.template nginx.conf.template
COPY ./vscode-reh vscode-reh

RUN apt-get update \
  && apt-get install -y nginx git \
  && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["./scripts/start.sh"]
